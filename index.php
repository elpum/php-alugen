<?php
include_once 'phpALUGen.php';

$allProds = $phpALUGen_ProductLibrary->getAllProducts();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>License Software Wizard</title>
</head>
<body>


<div style="text-align: center;">
    <h1>License Software Wizard</h1>

    <div style="text-align: left;">
        <form method="post" action="manager.php" name="Main">
            <table>
                <tr>
                    <td><label>Firstname</label></td>
                    <td><input name="First_Name" type="text"/></td>
                </tr>
                <tr>
                    <td><label>Lastname</label></td>
                    <td><input name="Surname" type="text"/></td>
                </tr>
                <tr>
                    <td><label>Company</label></td>
                    <td><input name="Company" type="text"/></td>
                </tr>
                <tr>
                    <td><label>Email</label></td>
                    <td><input name="Email_Address" type="text"/></td>
                </tr>
                <tr>
                    <td><label>Product</label></td>
                    <td>
                        <select>
                            <?php
                            foreach ($allProds as $curProduct) {
                                $prodString = base64_encode(rawurlencode(serialize(array($curProduct->Name, $curProduct->Version))));
                                echo '							<option value="' . $prodString . '">' . $curProduct->Name . ' - ' . $curProduct->Version . '</option>' . PHP_EOL;
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input name="Submit" value="Generate license" type="submit"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>
