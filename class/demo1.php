<?php
include('gksException.class.php');
include('encrypt.class.php');

try {
    $passwords = gksEncrypt::generateKeys(512);
} catch (gksException $E) {
    echo $E->getLogMessage();
    exit;
}

?>
<title>Generating RSA public/private keypair</title>
<h4>Public</h4>
<pre style="border: 1px solid black;width: 700px;"><?php echo $passwords['public']; ?></pre>
<h4>Private</h4>
<pre style="border: 1px solid black;width: 700px;"><?php echo $passwords['private']; ?></pre>
