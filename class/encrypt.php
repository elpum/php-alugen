<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: demo2.html');
    exit;
}

include('gksException.class.php');
include('encrypt.class.php');

$public_key = $_POST['public_key'];
$data = $_POST['data'];

try {
    /**
     * Constructor checks the server configuraton.
     * Use a static call gksEncrypt::encrypt($data,$public_key) if you want.
     */
    $ENC = new gksEncrypt();
    $ENC->setPublicKey($public_key);
    $enctrypted = $ENC->encrypt($data);
} catch (gksException $E) {
    echo $E->getLogMessage();
    exit;
}
$enctrypted = base64_encode($enctrypted);
?>
<title>Data encryption</title>

The encrypted base64-encoded data is:
<form action="demo3.php" method="post">
    <input type="text" name="encrypted_data" size="80" value="<?php echo $enctrypted; ?>"><br/>
    <input type="submit" value="decrypt">

</form>