<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: demo3.php');
    exit;
}

include('gksException.class.php');
include('encrypt.class.php');

$private_key = $_POST['private_key'];
$enctrypted_data = base64_decode($_POST['encrypted_data']);

try {
    $data = gksEncrypt::decrypt($enctrypted_data, $private_key);
} catch (gksException $E) {
    echo $E->getLogMessage();
    exit;
}

?>
<title>Data decryption</title>

The original data is:
<div style="border: 1px solid black;"><?php var_dump($data); ?></div>