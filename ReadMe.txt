php alugen for 3.6.1

Aim to update  php Alugen 3.4.1 working with Activelock.dll 3.5.4 to Activelock.dll 3.6.1

PHP Version 5.5.6
MySql Server version: 5.5.32
using portable Xampp
+ xdebug
+ firefox with easy xdebug 1.5 + easy xdebug (with moveable icon) 0.7.0 extensions (? not certain if both needed) 
+ notepad++ with DBGp extension

History
written Andy Schmitz
numerous people have had involvement
last one of significance oelly and my starting point http://elpum.com/activelockphp_20130313.zip

I do not need all the functions of php Alugen to be working
A minimum set is
Import products.ini
License generation
Product codes are imported from PC Alugen exported to-> licences.ini,  then imported into -> php Alugen on server.
Liberation key generation is the central requirement, which can be downloaded to customer.

A lot more has been implemented
The RSA style codes have been added and tested.
The old style code is still present and lightly tested.
Import and export products via products.ini functions.
Produce product codes implemented but lightly tested.
License generation using new rsa style codes tested.

Demo function ignored but still present
Update checks disabled as it hangs, server there but last updated 2006 for 3.4.1.
The following steps were required
Update to php 5.x, mainly change $var to $_POST['var'], 90% of bugs
Php is migrating from Mysql module mysql to mysqli. So this was also done. No errors in original, just a modernisation. 0% of bugs but quite a bit of work
I imported my licence.ini for my product codes. This did not contain the pseudo product phpalugen_admin_config  9% of bugs
nor did I initially understand what this product was all about. Big mistake, big problems. Hopefully, now if one doesn't exist a default one is generated.
Set $admin_config['noUpdateCheck'] = 'true' string not bool. I don't think server is active and caused problems.
Minor bugs in new RSA licence generation. All the difficult bit of product code generation was correct and forgetting to set vcode and gcode on return. 1% of bugs
Anything more and I would have been out of my depth. This was the difficult bit and had been completed before I started this exercise, well done.

phpalugen_admin_config
There are a few  program parameters, mainly  connected with the demo facility. These params are collected together as param1=x&param2=y&param3= z...
and then stored in gcode of the product phpalugen_admin_config and then retrieved from here on each activation.
If any problem here and your license.ini does not have a phpalugen_admin_config then add to products ini and import

[phpalugen_admin_config 1.0]
Name=phpalugen_admin_config
Version=1.0
VCode=
GCode=dXNlciZzJTNBNSUzQSUyMmFkbWluJTIyJTNCCnBhc3MmcyUzQTUlM0ElMjJhZG1pbiUyMiUzQgpkZW1vX2VuYWJsZSZzJTNBMiUzQSUyMm9uJTIyJTNCCmRlbW9fZGF5cyZzJTNBMiUzQSUyMjMwJTIyJTNCCmRlbW9fcHJvZHVjdHMmYSUzQTIlM0ElN0JpJTNBMCUzQnMlM0E3MiUzQSUyMmElMjUzQTIlMjUzQSUyNTdCaSUyNTNBMCUyNTNCcyUyNTNBNyUyNTNBJTI1MjJUZXN0QXBwJTI1MjIlMjUzQmklMjUzQTElMjUzQnMlMjUzQTElMjUzQSUyNTIyMyUyNTIyJTI1M0IlMjU3RCUyMiUzQmklM0ExJTNCcyUzQTc0JTNBJTIyYSUyNTNBMiUyNTNBJTI1N0JpJTI1M0EwJTI1M0JzJTI1M0E3JTI1M0ElMjUyMlRlc3RBcHAlMjUyMiUyNTNCaSUyNTNBMSUyNTNCcyUyNTNBMyUyNTNBJTI1MjIzLjYlMjUyMiUyNTNCJTI1N0QlMjIlM0IlN0QKZGVtb19sb2cmcyUzQTIlM0ElMjJvbiUyMiUzQgpkZW1vX2xvZ19kYXlzJnMlM0ExJTNBJTIyMCUyMiUzQgpkZW1vX2Jsb2NrJnMlM0EyJTNBJTIyb24lMjIlM0IKZGVtb19ibG9ja19kYXlzJnMlM0ExJTNBJTIyMCUyMiUzQgpub1VwZGF0ZUNoZWNrJnMlM0E0JTNBJTIydHJ1ZSUyMiUzQgo=


The code provided 
1. products.ini is not what I have been using
2. Config.inc.php  is not what I have been using
Both of these contain my passwords

You MUST run install.php first to create a Config.inc.php - not easy!
Use product.ini provided. I tested with ALTestApp 3.6, Alugen3.6 and Activelock_3.6.net.dll        


My interest is to update my online licensing from 3.5.4 to 3.6.1. This online licensing plugs into phpAlugen via the following procedure
3.5.4 versions
function processIC($license, $installcode, &$liberationKey) {
	// Get a liberation key
	$phpALUGen = new phpALUGen;
	$liberationKey = $phpALUGen->genKey($license, $installcode);
}
The license is created via the procedure
function setupLicense($productName, $productVersion, $registeredLevel, $licenseType, &$license) {
	$license->ProductName = $productName;
	$license->ProductVer = $productVersion;
	$license->RegisteredLevel = $registeredLevel;
	$license->LicenseClass = 'Single';
	$license->LicenseType = $licenseType;
	$dateFormat = 'Y/m/d';                              // hardcoded to latest release
	$license->RegisteredDate = gmdate($dateFormat);
	$license->MaxCount = '1';

	if ($license->LicenseType == 1) { // Periodic
		$license->Expiration = gmdate($dateFormat, time()+$licensetype1_exp*24*60*60);
	} elseif ($license->LicenseType == 3) { // Time Locked
		$license->Expiration = $licensetype3_exp;
	}
}
