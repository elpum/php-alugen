<?php
/**
 * Helper function - Designed for readability
 * @var $marker - typically output of Retrieve Product or similar function
 * could do with a file common.php for functions like this
 */
define('NOT_FOUND', false);
function Found($marker)
{
    return !($marker === false);
}

function NotFound($marker)
{
    return $marker === false;
}

?>