<?php
function adodb_mktime($hr, $min, $sec, $mon = false, $day = false, $year = false, $is_dst = false, $is_gmt = false)
{
    if (!defined('ADODB_TEST_DATES')) {

        if ($mon === false) {
            return $is_gmt ? @gmmktime($hr, $min, $sec) : @mktime($hr, $min, $sec);
        }

        // for windows, we don't check 1970 because with timezone differences,
        // 1 Jan 1970 could generate negative timestamp, which is illegal
        $usephpfns = (1971 < $year && $year < 2038
            || !defined('ADODB_NO_NEGATIVE_TS') && (1901 < $year && $year < 2038)
        );


        if ($usephpfns && ($year + $mon / 12 + $day / 365.25 + $hr / (24 * 365.25) >= 2038)) $usephpfns = false;

        if ($usephpfns) {
            return $is_gmt ?
                @gmmktime($hr, $min, $sec, $mon, $day, $year) :
                @mktime($hr, $min, $sec, $mon, $day, $year);
        }
    }

    $gmt_different = ($is_gmt) ? 0 : adodb_get_gmt_diff($year, $mon, $day);

    /*
    # disabled because some people place large values in $sec.
    # however we need it for $mon because we use an array...
    $hr = intval($hr);
    $min = intval($min);
    $sec = intval($sec);
    */
    $mon = intval($mon);
    $day = intval($day);
    $year = intval($year);


    $year = adodb_year_digit_check($year);

    if ($mon > 12) {
        $y = floor(($mon - 1) / 12);
        $year += $y;
        $mon -= $y * 12;
    } else if ($mon < 1) {
        $y = ceil((1 - $mon) / 12);
        $year -= $y;
        $mon += $y * 12;
    }

    $_day_power = 86400;
    $_hour_power = 3600;
    $_min_power = 60;

    $_month_table_normal = array("", 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $_month_table_leaf = array("", 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    $_total_date = 0;
    if ($year >= 1970) {
        for ($a = 1970; $a <= $year; $a++) {
            $leaf = _adodb_is_leap_year($a);
            if ($leaf == true) {
                $loop_table = $_month_table_leaf;
                $_add_date = 366;
            } else {
                $loop_table = $_month_table_normal;
                $_add_date = 365;
            }
            if ($a < $year) {
                $_total_date += $_add_date;
            } else {
                for ($b = 1; $b < $mon; $b++) {
                    $_total_date += $loop_table[$b];
                }
            }
        }
        $_total_date += $day - 1;
        $ret = $_total_date * $_day_power + $hr * $_hour_power + $min * $_min_power + $sec + $gmt_different;

    } else {
        for ($a = 1969; $a >= $year; $a--) {
            $leaf = _adodb_is_leap_year($a);
            if ($leaf == true) {
                $loop_table = $_month_table_leaf;
                $_add_date = 366;
            } else {
                $loop_table = $_month_table_normal;
                $_add_date = 365;
            }
            if ($a > $year) {
                $_total_date += $_add_date;
            } else {
                for ($b = 12; $b > $mon; $b--) {
                    $_total_date += $loop_table[$b];
                }
            }
        }
        $_total_date += $loop_table[$mon] - $day;

        $_day_time = $hr * $_hour_power + $min * $_min_power + $sec;
        $_day_time = $_day_power - $_day_time;
        $ret = -($_total_date * $_day_power + $_day_time - $gmt_different);
        if ($ret < -12220185600) $ret += 10 * 86400; // if earlier than 5 Oct 1582 - gregorian correction
        else if ($ret < -12219321600) $ret = -12219321600; // if in limbo, reset to 15 Oct 1582.
    }
    //print " dmy=$day/$mon/$year $hr:$min:$sec => " .$ret;
    return $ret;
}

?>