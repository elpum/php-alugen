<?php
include 'header.php';
// Get the encryption details
//$EncryptionType = $_POST['crypttype'];

error_reporting(-1);
ini_set("display_errors", 1);

$EncryptionType = 'RSA';
$LengthRequired = $_POST['depth'];

// Create the product
$newProduct = new phpALUGen_ProductInfo;

// Generate encryption keys for the product
$generator = new phpALUGen;
$generator->genProductKeys($EncryptionType, $LengthRequired, $VCode, $GCode);
$newProduct->VCode = $VCode;
$newProduct->GCode = $GCode;

//var_dump($GCode);
//var_dump($VCode);

// Set the product's name and version
$newProduct->Name = $_POST['prodName'];
$newProduct->Version = $_POST['prodVer'];

// Add the product
$phpALUGen_ProductLibrary->addProduct($newProduct);
?><h1>Product Has Been Created</h1>

<b><a href="prodKeys.php">Go back to Product Code Generator</a></b>
<br>

<?php
include 'footer.php';
?>

