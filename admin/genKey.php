<?php
include 'header.php';
include 'adodb-time.inc.php';

ini_set('precision', 15);

if ($_POST['strictCompat'] == '3.3') {
    $dateFormat = 'Y/m/d H:i:s';
} elseif ($_POST['strictCompat'] == '3.6') {
    $dateFormat = 'Y/m/d';
    $dbl_time = (time() - adodb_mktime(0, 0, 0, 12, 30, 1899)) / (24 * 60 * 60);
    $dbl_time_exp = (adodb_mktime(0, 0, 0, 1, 1, 9999) - adodb_mktime(0, 0, 0, 12, 30, 1899)) / (24 * 60 * 60);
} else {
    $dateFormat = 'Y/m/d';
}

$prodParts = unserialize(urldecode($_POST['product']));

$license = new phpALUGen_License;
$license->ProductName = $prodParts[0];
$license->ProductVer = $prodParts[1];
$license->RegisteredLevel = $_POST['RegisteredLevel'];
$license->RegisteredDate = $dbl_time;
$license->LicenseClass = 'Single';
$license->LicenseType = $_POST['LicenseType'];
if ($license->LicenseType == 1) { // Periodic
    $license->Expiration = gmdate($dateFormat, time() + $_POST['licensetype1_exp'] * 24 * 60 * 60);
    $license->Expiration = ((time() + ($_POST['licensetype1_exp'] * 24 * 60 * 60)) - adodb_mktime(0, 0, 0, 12, 30, 1899)) / (24 * 60 * 60);
} elseif ($license->LicenseType == 3) { // Time Locked
    $license->Expiration = (strtotime($_POST['licensetype3_exp']) - adodb_mktime(0, 0, 0, 12, 30, 1899)) / (24 * 60 * 60);
} else {
    $license->Expiration = $dbl_time_exp;
}
$license->MaxCount = '1';

// Get a liberation key
$phpALUGen = new phpALUGen;
$liberationKey = $phpALUGen->genKey($license, $_POST['InstallationCode']);

// Write out the liberation key
echo '<h2>Liberation Key:</h2><textarea rows="8" cols="96" readonly="readonly">' . $liberationKey . '</textarea><br><br>';
?>
<b><a href="licenseKeys.php">Go back to License Key Generator</a></b>
<?php
include 'footer.php';
?>
