<?php
include 'header.php';

// Retrieve the product
$thisProd = $phpALUGen_ProductLibrary->retrieveProduct($_GET['name'], $_GET['ver']);
?><b>Product Information:</b><br/>
<br/>
<table>
    <tr>
        <td>Name</td>
        <td><?php echo $thisProd->Name; ?></td>
    </tr>
    <tr>
        <td>Version</td>
        <td><?php echo $thisProd->Version; ?></td>
    </tr>
    <tr>
        <td>VCode</td>
        <td><textarea cols=75 readonly="readonly"><?php echo $thisProd->VCode; ?></textarea></td>
    </tr>
    <tr>
        <td>GCode</td>
        <td><textarea cols=75 readonly="readonly"><?php echo $thisProd->GCode; ?></textarea></td>
    </tr>
</table>
<b><a href="prodKeys.php">Go back to Product Code Generator</a></b>
<?php
include 'footer.php';
?>
