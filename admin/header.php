<?php
require_once('functions.php');

// This makes sure we don't get errors for accessing variables we haven't declared.
// Report all errors except E_NOTICE
// This is the default value set in php.ini
error_reporting(-1);
ini_set('display_errors', true);

$version = '3.6.1';
$tagline = 'Works with PHP 5.3 and up';

// This counteracts register_globals = off in php.ini
/*
if (!ini_get('register_globals')) {
	$types_to_register = array('GET','POST','COOKIE','SESSION','SERVER');
	foreach ($types_to_register as $type) {
		if (@count(${'HTTP_' . $type . '_VARS'}) > 0) {
			extract(${'HTTP_' . $type . '_VARS'}, EXTR_OVERWRITE);
		}
	}
}
*/
// This disables the login check. Set this to 1 if you forget your username or password.
//  Otherwise, set it to 0. DO NOT COMMENT OUT THIS LINE.
$noLogin = 1;

$noHeader = false;
$admin_config['noUpdateCheck'] = 'true';  ////dw2 there is no server to do checking - note this is string - could be a mistake
// ~~~~~ LIBRARY ~~~~~
$curCwd = getcwd();
chdir('..');
include_once 'phpALUGen.php';
chdir($curCwd);

// ~~~~~ CONFIG ~~~~~
// phpalugen_admin_config is not a product - some values (configurationSave.php) are stuffed in the GCode
// simpler (?) than reading writing to a dedicated file
// funny number below produces a default configuration
$configstr = $phpALUGen_ProductLibrary->retrieveProduct('phpalugen_admin_config', '1.0');
if (NotFound($configstr)) $configstr = new phpALUGen_ProductInfo;

if (!isset($configstr->GCode)) $configstr->GCode = 'dXNlciZzJTNBNSUzQSUyMmFkbWluJTIyJTNCCnBhc3MmcyUzQTUlM0ElMjJhZG1pbiUyMiUzQgpkZW1vX2VuYWJsZSZzJTNBMiUzQSUyMm9uJTIyJTNCCmRlbW9fZGF5cyZzJTNBMiUzQSUyMjMwJTIyJTNCCmRlbW9fcHJvZHVjdHMmYSUzQTIlM0ElN0JpJTNBMCUzQnMlM0E3MiUzQSUyMmElMjUzQTIlMjUzQSUyNTdCaSUyNTNBMCUyNTNCcyUyNTNBNyUyNTNBJTI1MjJUZXN0QXBwJTI1MjIlMjUzQmklMjUzQTElMjUzQnMlMjUzQTElMjUzQSUyNTIyMyUyNTIyJTI1M0IlMjU3RCUyMiUzQmklM0ExJTNCcyUzQTc0JTNBJTIyYSUyNTNBMiUyNTNBJTI1N0JpJTI1M0EwJTI1M0JzJTI1M0E3JTI1M0ElMjUyMlRlc3RBcHAlMjUyMiUyNTNCaSUyNTNBMSUyNTNCcyUyNTNBMyUyNTNBJTI1MjIzLjYlMjUyMiUyNTNCJTI1N0QlMjIlM0IlN0QKZGVtb19sb2cmcyUzQTIlM0ElMjJvbiUyMiUzQgpkZW1vX2xvZ19kYXlzJnMlM0ExJTNBJTIyMCUyMiUzQgpkZW1vX2Jsb2NrJnMlM0EyJTNBJTIyb24lMjIlM0IKZGVtb19ibG9ja19kYXlzJnMlM0ExJTNBJTIyMCUyMiUzQgpub1VwZGF0ZUNoZWNrJnMlM0E0JTNBJTIydHJ1ZSUyMiUzQgo=';
$configstr = base64_decode($configstr->GCode);
$configlines = explode("\n", $configstr);
foreach ($configlines as $configline) {
    $configparts = explode('&', $configline);
    if (count($configparts) == 2) {
        $admin_config[urldecode($configparts[0])] = unserialize(urldecode($configparts[1]));
    }
}

// Make sure the products set is an array
// don't understand next line so best to ensure it is an array - the value above is good for that
if (!is_array($admin_config['demo_products'])) {
    $admin_config['demo_products'][] = $admin_config['demo_products'];
}

// If there is no username or password, make login not required
if (($admin_config['user'] == '') && ($admin_config['pass'] == '')) {
    $noLogin = 1;
}

if ($noHeader) {
    return;
}

// ~~~~~ LOGIN ~~~~~
// Our 'not logged in function'.
function notLoggedIn()
{
    global $noLogin;
    if ($noLogin != 1) {
        header('WWW-Authenticate: Basic realm="phpALUGen Administration"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Please Login Correctly. If you have forgotten your password, edit header.php and set $noLogin to 1.';
        exit;
    }
}

// Make sure the user is logged in
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    notLoggedIn();
} else {
    if (($_SERVER['PHP_AUTH_USER'] != $admin_config['user']) || ($_SERVER['PHP_AUTH_PW'] != $admin_config['pass'])) {
        notLoggedIn();
    }
}
?>
<html>
<head>
    <title>ActiveLock Universal GENerator</title>
    <style type="text/css">H1 {
            font-family: Verdana, Arial, sans-serif;
            font-size: 27px;
            line-height: 1.5;
        }

        H2 {
            font-family: Verdana, Arial, sans-serif;
            font-size: 18px;
            line-height: 1.5;
        }

        TD, UL, P, BODY {
            font-family: Verdana, Arial, sans-serif;
            font-size: 11px;
            line-height: 1.5;
        }

        TABLE.installForm {
            border-right: #cccccc 1px solid;
            border-top: #cccccc 1px solid;
            border-left: #cccccc 1px solid;
            border-bottom: #cccccc 1px solid;
            background-color: #ffffee;
        }</style>
</head>
<body>
<center>
    <?php
    // Check for an upgrade
    if ($admin_config['noUpdateCheck'] != 'true') {
        if (filemtime('updatecheck.txt') < (time() - 60 * 60)) {
            $updateText = @file_get_contents('http://lardbucket.org/projects/activelock/updatecheck.php?v=' . $version);
            $fp = fopen('updatecheck.txt', 'w');
            fwrite($fp, $updateText);
            fclose($fp);
        } else {
            $updateText = file_get_contents('updatecheck.txt');
        }
        if ($updateText && ($updateText != 'NOUP')) {
            echo $updateText;
        }
    }
    ?>
    <table width="95%" class="installForm">
        <tr>
            <td width="30%">
                <center><a href="prodKeys.php"><b>Product Code Generator</b></a></center>
            </td>
            <td width="30%">
                <center><a href="licenseKeys.php"><b>License Key Generator</b></a></center>
            </td>
            <td width="30%">
                <center><a href="configuration.php"><b>Configure</b></a></center>
            </td>
            <td width="10%">
                <center><img border="0" src="small-logo.png"/><br/>phpALUGen <?php echo $version; ?>
                    <br/><?php echo $tagline; ?></center>
            </td>
        </tr>
    </table>
    <br/>
    <table width="95%" class="installForm">
        <tr>
            <td>
