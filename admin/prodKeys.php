<?php
include 'header.php';

// not used
//$depth = isset($_GET['depth']) == true ? $_GET['depth'] : '';
//$crypttype = isset($_GET['crypttype']) == true ? $_GET['crypttype'] : '';
?>

<?php
$allProds = $phpALUGen_ProductLibrary->getAllProducts();
?>
<b>Create New Product:</b>
<form name="prodKeys" method="POST" action="addProduct.php">
    <input name="depth" id="RSA4096" value="4096" type="radio"><label for="RSA4096">RSA4096</label></input>
    <input name="depth" id="RSA2048" value="2048" type="radio"><label for="RSA2048">RSA2048</label></input>
    <input name="depth" id="RSA1536" value="1536" type="radio"><label for="RSA1536">RSA1536</label></input>
    <input name="depth" id="RSA1024" value="1024" type="radio" checked="checked"><label
        for="RSA1024">RSA1024</label></input>
    <input name="depth" id="RSA512" value="512" type="radio"><label for="RSA512">RSA512</label></input>
    <br>
    <table>
        <tr>
            <td>
                Name:
            </td>
            <td>
                <input type="text" name="prodName"></input>
            </td>
        </tr>
        <tr>
            <td>
                Version:
            </td>
            <td>
                <input type="text" name="prodVer"></input>
            </td>
        </tr>
    </table>
    <br>


    <input type="submit" value="Create Keys and Add Product"></input>


</form>

</td></tr></table><br/>
<table width="95%" class="installForm">
    <tr>
        <td>

            <b>Product List:</b>
            <table cellspacing=0 cellpadding=0 border=0>
                <tr>
                    <td colspan="13" bgcolor="black" height="1">
                </tr>
                <tr>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        Name
                    </td>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        Version
                    </td>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        VCode
                    </td>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        GCode
                    </td>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        &nbsp;View Information&nbsp;
                    </td>
                    <td width="1" bgcolor="black"></td>
                    <td>
                        &nbsp;Remove&nbsp;
                    </td>
                    <td width="1" bgcolor="black"></td>
                </tr>
                <tr>
                    <td colspan="13" bgcolor="black" height="1">
                </tr>
                <?php
                foreach ($allProds as $curProduct) {
                    ?>
                    <tr>
                        <td width="1" bgcolor="black"></td>
                        <td>
                            <?php echo $curProduct->Name; ?>
                        </td>
                        <td width="1" bgcolor="black"></td>
                        <td>
                            <?php echo $curProduct->Version; ?>
                        </td>
                        <td width="1" bgcolor="black"></td>
                        <td>
                            <?php echo substr($curProduct->VCode, 0, 20); ?>...
                        </td>
                        <td width="1" bgcolor="black"></td>
                        <td>
                            <?php echo substr($curProduct->GCode, 0, 20); ?>...
                        </td>
                        <td width="1" bgcolor="black"></td>
                        <td>

                            <center><a
                                    href="viewInfo.php?name=<?php echo urlencode($curProduct->Name) ?>&ver=<?php echo urlencode($curProduct->Version); ?>">View</a>
                            </center>

                        </td>
                        <td width="1" bgcolor="black"></td>
                        <td>
                            <center><a
                                    href="remove.php?name=<?php echo urlencode($curProduct->Name) ?>&ver=<?php echo urlencode($curProduct->Version); ?>">Remove</a>
                            </center>
                        </td>
                        <td width="1" bgcolor="black"></td>
                    </tr>
                    <tr>
                        <td colspan="13" bgcolor="black" height="1">
                    </tr>
                <?php } ?>
            </table>
            <br/>
            <b><a href="productsIni.php">Export to products.ini format</a></b><br/>
            <b><a href="importIni.php">Import from products.ini format</a></b>

            <?php
            include 'footer.php';
            ?>
